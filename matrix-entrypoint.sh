#!/usr/bin/env bash
RETRIES=10
until psql -h matrixdb -U matrix -d squiz_matrix -c "select 1" > /dev/null 2>&1 || [ $RETRIES -eq 0 ]; do
  echo "Waiting for postgres server, $((RETRIES--)) remaining attempts..."
  sleep 1
done

if psql -h matrixdb -U matrix -d squiz_matrix -c "SELECT 'public.sq_ast'::regclass" ; then
  echo "A squiz install already exists, continue on"
else
  echo "Install Matrix as one doesnt exist"
  php /var/www/squiz_matrix/install/step_02.php /var/www/squiz_matrix
  php /var/www/squiz_matrix/install/step_03.php /var/www/squiz_matrix
fi
sleep 3s

/etc/init.d/apache2 start
while :; do :; done & kill -STOP $! && wait $!
