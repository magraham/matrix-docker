#!/usr/bin/env bash
createuser -SRDU postgres matrix
createuser -SRDU postgres matrix_secondary
createdb -U postgres -O matrix -E UTF8 squiz_matrix
psql -U postgres -d squiz_matrix -c "ALTER USER matrix WITH PASSWORD 'matrixpass'"
psql -U postgres -d squiz_matrix -c "ALTER USER matrix_secondary WITH PASSWORD 'matrixpass'"
pg_ctl -D /var/lib/postgresql/data -l logfile start