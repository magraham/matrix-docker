<?php
$db_conf = array (
        'db' => array (
                'DSN' => 'pgsql:dbname=squiz_matrix;host=matrixdb',
                'user' => 'matrix',
                'password' => 'matrixpass',
                'type' => 'pgsql',
               ),
        'db2' => array (
                'DSN' => 'pgsql:dbname=squiz_matrix;host=matrixdb',
                'user' => 'matrix',
                'password' => 'matrixpass',
                'type' => 'pgsql',
               ),
        'db3' => array (
                'DSN' => 'pgsql:dbname=squiz_matrix;host=matrixdb',
                'user' => 'matrix_secondary',
                'password' => 'matrixpass',
                'type' => 'pgsql',
               ),
        'dbcache' => NULL,
        'dbsearch' => NULL,
        );

return $db_conf;
?>
