Use  `docker-compose up -d` to bring up the cluster

If you want to build the matrix application separately:
Use `docker build -f dockerfile.matrix -t matrixapp .` to build the matrix application image

If you want to build the matrix db separately:
Use `docker build -f dockerfile.postgres -t matrixdb .` to build the matrix db image
